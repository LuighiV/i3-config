# Configuration of I3WM

This is my configuration for i3WM. It has some special settings for operation
and for keybindings in order to facilitate the workflow in i3WM.

## Applications employed

- Compositor: picom
- Desktop wallpaper: nitrogen
- Locker: i3-lock
- Network manager: nm-applet
- Sound control: pa-applet
- Bluetooth control: blueman-applet
- Battery manager: xfce4-power-manager
- Application manager: xfce4-appfinder

### Additional applications at startup

I use some applications at startup which help me in my daily work:

- Cloud storage:
  - megasync
  - nextcloud
  - onedrive --monitor

- Note taker:
  - nixnote2

- Password manager:
  - keepassxc

- Chat ad workspace:
  - teams


